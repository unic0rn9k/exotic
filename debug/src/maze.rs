use anyhow::*;
use rand::*;
use crossterm::{
    ExecutableCommand, QueueableCommand,
    terminal, cursor, style::{self, Colorize, Styler}
};
use std::io::{stdout, Write};

use exotic::{*, prelude::*};
use exotic_macro::*;
use config::*;
use lazy_static::*;

lazy_static!{
    pub static ref DIRS: [fn(usize) -> usize;4] = [
        |x| x + 1,
        |x| x - 1,
        |x| x + WIDTH,
        |x| x - WIDTH,
    ];
}

pub const WIDTH      : usize   = 46;
pub const HEIGHT     : usize   = 46;

pub const LR         : Float   = 0.1;
pub const DISCOUNT   : Float   = 0.9;
pub const QDEPTH     : usize   = 5;
pub const FEAR       : Float   = -5.0;

//pub const EPSILON    : Float   = 100.0;
//pub const CURIOSITY  : Float   = 2.0;
//pub const POINT      : Float   = 10.0;

macro_rules! entities{
    ($($entity: ident $value: expr),* $(,)?) => {
        #[derive(Copy, Clone, PartialEq)]
        pub enum Entity{$(
            $entity,
        )*}
        use Entity::*;
        #[derive(Copy, Clone)]
        struct EntityAxon<'a, const LEN: usize>(pub &'a [Entity; LEN]);
        impl<'a, const LEN: usize> std::ops::Index<usize> for EntityAxon<'a, LEN>{
            type Output = Float;
            fn index(&self, n: usize) -> &Float{
                match self.0[n]{$(
                    $entity => &$value,
                )*}
            }
        }
        impl<'a, const LEN: usize> exotic::deep::Axon<LEN> for EntityAxon<'a, LEN>{
            fn zero() -> Self{
                panic!("Entity has no zero value, and cannot return reference")
            }
        }
    }
}

entities!{
    Background 0.,
    Robot 0.,
    Wall -5.,
    Trail -1.,
}

fn top(a: &[Float]) -> usize{
    let mut mx = 0;
    for n in 1..a.len(){
        if a[n] > a[mx]{
            mx=n
        }
    }
    mx
}

#[derive(Clone, Copy)]
pub struct Point{
    pub x: usize,
    pub y: usize,
}

impl Point{
    pub fn step(mut self, dir: usize) -> Self{
        match dir{
            0 => self.x += 1 * (self.x < WIDTH) as usize,
            1 => self.y += 1 * (self.y < HEIGHT) as usize,
            2 => self.y -= 1 * (self.y > 0) as usize,
            3 => self.x -= 1 * (self.x > 0) as usize,
            d => panic!("Direction ({}) should be between 0 and 3", d)
        }
        self
    }
    pub fn index(&self) -> usize{
        self.x+self.y*WIDTH
    }
}

fn maxq(q: &[[Float;4];WIDTH*HEIGHT], mut i: Point) -> Float{
    let mut reward = 0.0;

    for n in 0..QDEPTH.min(WIDTH*HEIGHT){
        let next = top(&q[i.index().min(q.len()-1)]);
        reward += q[i.index().min(q.len()-1)][next] * DISCOUNT.powi(n as i32);
        i = i.step(next);
    }

    reward
}

/*
fn maxdq(dnn: &dyn Layer, mut i: Point, mut state: [Float; WIDTH*HEIGHT]) -> Result<Float>{
    let mut reward = 0.0;

    for n in 0..QDEPTH.min(WIDTH*HEIGHT){
        let q = dnn.predict(&area(&state, i)[..])?;
        let next = top(&q);
        reward += q[next] * DISCOUNT.powi((n + 1) as i32);
        let new = i.step(next);
        buffer_sim(&mut state, i.index(), new.index());
        i = new;
    }

    Ok(reward)
}
*/

fn buffer_sim(buffer: &mut [Entity], old: usize, mut new: usize){
        buffer[old] = Trail;
        if buffer[new] == Wall{
            new = old;
        }
        buffer[new] = Robot;
}

fn preocess_frames(frames: &[[Float;WIDTH*HEIGHT]]) -> Vec<Float>{
    let mut i = vec![];
    i.extend_from_slice(&frames[0][..]);
    i.extend_from_slice(&frames[1][..]);
    i.extend_from_slice(&frames[2][..]);
    i
}

/*
fn DeepQ(dnn: &DNN, p: usize, two_states: &mut [[Float;WIDTH*HEIGHT]]) -> Result<[Float;4]>{
    let mut tmp = [0.0;4];
    for (dir, tmp) in DIRS.iter().zip(tmp.iter_mut()){
        let dir = dir(p);
        two_states[2]=two_states[1];
        buffer_sim(&mut two_states[2], p, dir);
        *tmp = dnn.predict( &preocess_frames(two_states)[..] )?[0]
    }
    Ok(tmp)
}

fn DeepQ<
    const I_LEN: usize,
    const LEN: usize,
    const O_LEN: usize,
>(
    dnn: &DNN<I_LEN, LEN, O_LEN>,
    p: usize,
    mut state: [Float;WIDTH*HEIGHT]
) -> Result<[Float;4]>{

    let mut tmp = [0.0;4];
    for (dir, tmp) in DIRS.iter().zip(tmp.iter_mut()){
        let dir = dir(p);
        buffer_sim(&mut state, p, dir);
        *tmp = dnn.predict( &state[..] )?[0]
    }
    Ok(tmp)
}
*/

fn area(buffer: &[Entity], p: Point) -> [Entity;100]{
    let mut tmp = [Background;100];
    let mut n = 0;
    for x in  (p.x as isize - 5) .. (p.x as isize + 5){
        for y in (p.y as isize - 5) .. (p.y as isize + 5){

            tmp[n] = if x < 0 || y < 0 || x as usize >= WIDTH || y as usize >= HEIGHT{
                Wall
            }else{
                buffer[x as usize + y as usize * WIDTH]
            };

            n+=1
        }
    }
    tmp
}

pub fn make_map() -> [Entity; WIDTH * HEIGHT]{
    let mut buffer  = [Background; WIDTH * HEIGHT];
    for n in 0..buffer.len(){
        if n < WIDTH{
            buffer[n] = Wall;
            continue
        }
        if n % WIDTH == 0{
            buffer[n] = Wall;
            continue
        }
        if n > WIDTH*(HEIGHT-1){
            buffer[n] = Wall;
            continue
        }
        if n % WIDTH == WIDTH-1{
            buffer[n] = Wall;
            continue
        }
        if n % (WIDTH + 1) == 0 || n % (WIDTH + 1) == 1{
            continue
        }

        if random::<Float>() < ( 4 - DIRS.iter().map(|dir|{
            if buffer[dir(n)] == Wall{
                return 1
            }
            0
        }).sum::<u8>()) as Float * 0.1{
            buffer[n] = Wall
        }
    }
    buffer
}

pub fn main() -> Result<()>{
    let mut buffer  = make_map();

    let mut qtable = [[0.0;4]; WIDTH * HEIGHT];
    let mut robot = Point{x: 1, y: 1};

    let format_table = |qtable: &[[Float; 4]; WIDTH*HEIGHT]|{
        let mut tmp = [0.; WIDTH*HEIGHT];
        qtable.iter().map(|n| n.iter().map(|n| n.abs() ).sum::<Float>()/4. ).zip(tmp.iter_mut()).for_each(|(q, p)| *p = q);
        format!("\n{:?}", matrix![&tmp, I![const WIDTH, HEIGHT]])
    };

    use exotic::deep::activation::*;
    use exotic::deep::DenseLayer;
    use exotic::deep::recurent;

    model!((
        name: "MaxqNet",
        cached: true,
        derive: Some(["Clone", "Copy"]),
        layers: [
            ( "DenseLayer",           [100, 80 ]  ),
            ( "Swish",                [80      ]  ),
            ( "recurent::DenseLayer", [80, 200 ]  ),
            ( "Tanh",                 [200      ] ),
            ( "DenseLayer",           [200, 4   ] ),
        ]
    ));
    
    let mut maxq_net = MaxqNet{
        l0: DenseLayer::random(0.005),
        l1: Swish,
        l2: recurent::DenseLayer::random(0.001),
        l3: Tanh,
        l4: DenseLayer::random(0.0005),
        //l1: Swish,
        //l2: DenseLayer::random(0.01),
        cache: MaxqNet::new_cache(),
    };

    let mut time = 0;
    let mut const_time = 0;
    let mut cost_sum = 0.0;
    let mut max_cost = 0.0;

    let mut stdout = stdout();
    stdout.execute(terminal::Clear(terminal::ClearType::All))?;

    let mut draw_map = |buffer: &mut [Entity; WIDTH * HEIGHT], stdout: &mut std::io::Stdout| -> Result<()>{
        for y in 0..HEIGHT{
            for x in 0..WIDTH{
                let mut pixel = |s| -> anyhow::Result<()> {stdout.queue(cursor::MoveTo(x as u16 * 2, y as u16))?.queue(style::PrintStyledContent(s))?; Ok(())};

                match buffer[x+y*WIDTH]{
                    Wall  => pixel("██".red())?,
                    Robot => pixel("@".cyan().bold())?,
                    Trail => pixel(". ".red())?,
                    _ => pixel("  ".white())?,
                };
            }
        }
        Ok(())
    };

    draw_map(&mut buffer, &mut stdout)?;

    loop{
        //println!("COST: {}", cost_sum/time as Float);
        stdout.flush()?;
        stdout.queue(cursor::MoveTo(robot.x as u16 * 2, robot.y as u16))?.queue(style::PrintStyledContent(". ".red()))?;
        
        let i = area(&buffer, robot);

        //let mut maxq_net_memory = [vec![], vec![]];
        //let o = maxq_net.predict_buffered(&i, &mut maxq_net_memory, &[false, true, false, false, false])?;
        let o = maxq_net.cache(EntityAxon(&i)).context(format_table(&qtable))?;

        let mut dcost = [0.0;4];
        let mut cost  = [0.0;4];

        let qx = robot.index();

        for action in 0..4{
            let mut new_robot = robot.step(action);
            let mut reward = 0.0;

            if buffer[new_robot.index()] == Wall{
                new_robot = robot;
                reward = FEAR;
            }else
            if buffer[new_robot.index()] == Trail || buffer[new_robot.index()] == Robot{
                reward = FEAR;
            }

            reward = round(reward);

            //let maxq = maxdq(&maxq_net, new_robot, buffer.clone())?;
            let maxq = maxq(&qtable, new_robot);

            qtable[qx][action] = qtable[qx][action] * (1.0-LR) + LR * (reward + DISCOUNT * maxq);

            dcost[action] = -2.0 * (qtable[qx][action] - o[action]);
            cost[action] = (qtable[qx][action] - o[action]).powi(2);
        }

        let mut qrious = "";

        let qy = {
            let top_cost = top(&cost);
            max_cost += cost[top_cost];
            if cost[top_cost] > max_cost / (const_time as Float + 1.0) {
                qrious = "exploring";
                top_cost
            }else{
                top(&*o)
            }
        };

        //let qy = top(&o);

        cost_sum += cost[qy];
        time += 1;
        const_time += 1;
        //println!(" cost: {:.3} \t │ DeepMaxQ: {:.3} \t │ {}", cost_sum/time as Float, o[qy], qrious);

        match maxq_net.backpropagate_from_cache(EntityAxon(&i), dcost){
            Ok(_) => {},
            Err(e) => {
                stdout.execute(terminal::Clear(terminal::ClearType::All))?;
                let mut tmp = [0.; WIDTH*HEIGHT];
                qtable.iter().map(|n| n.iter().map(|n|n.abs()).sum() ).zip(tmp.iter_mut()).for_each(|(q, p)| *p = q);
                matrix![&tmp, I![const WIDTH, HEIGHT]].heatmap(50.)?;
                maxq_net.printcache();
                return Result::Err(e);
            }
        };
        //maxq_net.l2.buffer = maxq_net.cache.2;

        let old = robot;
        robot = robot.step(qy);
        if buffer[robot.index()] == Wall{
            robot = old;
        }

        buffer_sim(&mut buffer, old.index(), robot.index());

        if time > 4000{
            buffer = make_map();
            draw_map(&mut buffer, &mut stdout)?;
            robot  = Point{x:1, y:1};
            time = 0;
            cost_sum = 0.0;
            qtable = [[0.0;4];WIDTH*HEIGHT];
            //maxq_net.l2.forget();
        }
        stdout.queue(cursor::MoveTo(robot.x as u16 * 2, robot.y as u16))?.queue(style::PrintStyledContent("@ ".cyan()))?;
    }
}
