# Exotic

Exotic is a machine learning (and partly data science) library made for the best performance possible. The goal of exotic is to implement some of the major machine learning algorithms in rust, and with that eventually make rust the go-to choice for machine learning research.

## Notice

Exotic is subject to a lot of changes, as it is in the early stages of developement.

## Todo

- [X] Replace DNN struct with proc macro that generates struct for a model and implements Layer trait for it.

- [X] Use const generics in output of layer type instead of Box<_>

- [X] Make error handling for incorrect dimensions compile time evaluated

- [ ] Implement 2d convolution

- [ ] Implement nd convolution

- [ ] Better AVX optimization (SIMD vectorization for all layers and backprop)

# Why is there no documentation?

For now rustdoc can't compile documentation for crates that depend on ```const_evaluatable_checked```.
So I haven't worried to much about writting the documentation, since it won't compile anyway.
Once the issue is fixed I will be sure to document the crate.
For now there is some examples in the [debug](debug) workspace (it might be moved to [examples](examples) in the future).

[Issue causing crash](https://github.com/rust-lang/rust/issues/77647)
                                                                                       
