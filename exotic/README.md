[![gitlab]](https://gitlab.com/unic0rn9k/exotic)

[gitlab]: https://img.shields.io/badge/gitlab-FC6D27?style=for-the-badge&labelColor=555555&logo=gitlab

# About

Exotic is a machine learning (and partly data science) library made for the best performance possible. The goal of exotic is to implement some of the major machine learning algorithms in rust, and with that eventually make rust the go-to choice for machine learning research.

More information on gitlab.

# Why is there no documentation?

For now rustdoc can't compile documentation for crates that depend on ```const_evaluatable_checked```.
So I haven't worried to much about writting the documentation, since it won't compile anyway.
Once the issue is fixed I will be sure to document the crate.
For now there is some examples in the [debug](debug) workspace (it might be moved to [examples](examples) in the future).

[Issue causing crash](https://github.com/rust-lang/rust/issues/77647)

