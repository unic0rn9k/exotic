use crate::{
    config::*,
    deep::{DenseLayer, Neuron},
};
use std::iter::Iterator;
//use rayon::prelude::*;

/// This trait will be can be used to deserialize and serialize model.
/// Both serialization and deserialization must be implemeted for this trait.
/// The trait should only be implemented for structs that only contain data of type Float.
/// The bytes Iterator passed to the deserialization function might contain more data than needen
/// to deserialize Self, this is because the remaining bytes might be used to deserialize later
/// layers in a model.
/// deserialize should only return None if it runs out of bytes.
pub trait Serialization<const SIZE: usize>: Sized {
    fn serialize_into(&self, buffer: &mut [[u8; FLOAT_BYTES]]);
    fn deserialize(&mut self, bytes: &mut dyn Iterator<Item = u8>);
    fn serialize(&self) -> [[u8; FLOAT_BYTES]; SIZE] {
        let mut buffer = [[0u8; FLOAT_BYTES]; SIZE];
        self.serialize_into(&mut buffer);
        buffer
    }
}

const EOB: &str = "Ran out of bytes while deserializing.";

fn next_float(bytes: &mut dyn Iterator<Item = u8>) -> Option<Float> {
    let mut byte = [0u8; FLOAT_BYTES];
    for o in byte.iter_mut() {
        *o = bytes.next()?
    }
    Some(Float::from_ne_bytes(byte))
}

impl<const LEN: usize> Serialization<{ LEN + 1 }> for Neuron<LEN> {
    fn serialize_into(&self, buffer: &mut [[u8; FLOAT_BYTES]]) {
        for n in 0..LEN {
            buffer[n] = self.w[n].to_ne_bytes()
        }
        buffer[LEN] = self.b.to_ne_bytes();
    }

    fn deserialize(&mut self, bytes: &mut dyn Iterator<Item = u8>) {
        for n in 0..LEN {
            self.w[n] = next_float(bytes).expect(EOB)
        }
        self.b = next_float(bytes).expect(EOB);
    }
}

impl<const I_LEN: usize, const O_LEN: usize> Serialization<{ (I_LEN + 1) * O_LEN + 1 }>
    for DenseLayer<I_LEN, O_LEN>
{
    fn serialize_into(&self, buffer: &mut [[u8; FLOAT_BYTES]]) {
        assert!(buffer.len() == (I_LEN + 1) * O_LEN + 1);
        for n in 0..O_LEN {
            self.neurons[n].serialize_into(&mut buffer[n * (I_LEN + 1)..(n + 1) * (I_LEN + 1)])
        }
        *buffer.last_mut().unwrap() = self.lr.to_ne_bytes();
    }

    fn deserialize(&mut self, bytes: &mut dyn Iterator<Item = u8>) {
        for n in 0..O_LEN {
            self.neurons[n].deserialize(bytes)
        }
        self.lr = next_float(bytes).expect(EOB);
    }
}
