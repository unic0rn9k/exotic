use mnist::{Mnist, MnistBuilder};
use exotic_macro::*;
use exotic::{
    deep::{
        activation::*,
        DenseLayer,
        LazyAxon,
    },
    onehot,
    matrix,
    prelude::*,
};
use rand::Rng;

pub fn main() -> Result<()>{

    let Mnist { trn_img, trn_lbl, .. } = MnistBuilder::new()
        .base_path("mnist/mnist")
        .label_format_digit()
        .training_set_length(60_000)
        .finalize();

    model!((
        name: "Mnet",
        cached: true,
        layers: [
            ("DenseLayer", [784, 15]),
            ("Swish", [15]),
            ("DenseLayer", [15, 10]),
            ("Sigmoid", [10]),
        ]
    ));

    let mut mnet = Mnet{
        l0: DenseLayer::random(0.01),
        l1: Swish,
        l2: DenseLayer::random(0.01),
        l3: Sigmoid,
        cache: Mnet::new_cache(),
    };

    let img0: Vec<_> = trn_img[0..28*28].iter().map(|n| *n as f32 / 255.).collect();

    matrix![&img0[..], I![const 28, 28]].heatmap(1.)?;
    println!("{:?}", trn_lbl[0]);

    let image_data: Vec<_> = trn_img.iter().map(|n| *n as f32 / 255.).collect();
    drop(trn_img);

    const SIZE: usize = 28*28;

    let mut correct = [false; 100];

    for epoch in 0..200000{
        let n = rand::thread_rng().gen_range(0..trn_lbl.len());
        let i = &image_data[n*SIZE..(n+1)*SIZE];

        let o = mnet.cache(LazyAxon::<{Mnet::I_LEN}>(i))?.to_array();
        let y = onehot::<10>(trn_lbl[n] as usize);
        //println!("{:?}", y);

        let mut dcost = [0.;10];
        let mut cost = 0.;
        for n in 0..10{
            dcost[n] = o[n] - y[n];
            cost += (y[n] - o[n]).powi(2)
        }

        mnet.backpropagate_from_cache(LazyAxon::<{Mnet::I_LEN}>(i), dcost)?;
        
        if o.argmax().0 as u8 == trn_lbl[n]{
            correct[epoch % 100] = true
        }

        //mnet.printcache();
        if epoch % 100 == 0{
            //matrix![i, I![const 28, 28]].heatmap(1.)?;
            matrix![i, I![const 28, 28]].heatmap(1.)?;
            println!("cost {:.3}; epoch {}; accuracy {}%; {:.3?}", cost, epoch, correct.iter().map(|n|*n as u8).sum::<u8>(), o);
            correct = [false;100];
        }
    }
    for n in 0..10{
        matrix![&mnet.l0.neurons[n].w[..], I![const 28, 28]].heatmap(0.5)?;
    }
    Ok(())
}
