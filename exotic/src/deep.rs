use crate::config::*;
use anyhow::*;

#[cfg(feature = "random")] use rand::random;

#[cfg(not(feature = "random"))]
fn random<T: Default>() -> T{T::default()}

use rayon::prelude::*;
use std::ops::*;

#[cfg(feature = "prettyprint")]
use crossterm::{style, QueueableCommand};
use std::fmt::{self, Display};
use std::io::{stdout, Write};

#[macro_export]
macro_rules! axon {
    (ref $data: expr) => {{
        AxonRef::Ref($data)
    }};
    ($a: expr, $b: expr) => {{
        AxonChain($a, $b)
    }};
}

//pub mod par;
pub mod activation;
pub mod conv;
pub mod recurrent;

// Number Array Trait

pub trait Axon<const LEN: usize>:
    Copy + Sync + Index<usize, Output = Float> + Index<Range<usize>, Output = [Float]>
{
    /// Should return an axon filled with 0's of type Self.
    fn zero() -> Self;

    /// Copies self into an array. This is very slow, O(n), and should therefor be avoidet.
    fn to_array(&self) -> [Float; LEN] {
        let mut tmp = [0.; LEN];
        for n in 0..LEN {
            tmp[n] = self[n]
        }
        tmp
    }

    /// Returns self as a slice.
    /// This is fast, but will not work for some types, fx AxonChain's
    fn slice(&self) -> &[Float] {
        &self[0..LEN]
    }

    /// Returns an AxonRef to self.
    fn to_ref<'a>(&'a self) -> AxonRef<'a, Self, LEN> {
        AxonRef::Ref(self)
    }

    /// Returns self as an AxonRef that contains its own value (a fake ref).
    fn to_fake(self) -> AxonRef<'static, Self, LEN> {
        AxonRef::Fake(self)
    }

    /// Returns a ref to the bigest element, and its index.
    fn argmax(&self) -> (usize, &Float) {
        let mut max = 0;
        for n in 0..LEN {
            if self[n] > self[max] {
                max = n
            }
        }
        (max, &self[max])
    }

    // Simd function should return a simd array, from the original self index..index+LANES
    // This will likely be optional in the future, but required for (AVX) simd optimization
    //
    // ## Default performance
    //
    // It is important to note that the default implementation for the simd function is ***very***
    // slow, and should always be reimplemented for types individually.
    // This may change in the future, as Axons may depend on Index<Range<usize>>
    //fn simd(&self, index: usize) -> Simd<[Float; SIMD_LANES]>{
    //    let mut tmp = [0.; SIMD_LANES];
    //    for n in 0..SIMD_LANES{
    //        tmp[n] = self[index+n]
    //    }
    //    Simd::<[Float; SIMD_LANES]>::from_slice_unaligned(&tmp)
    //}
}

/// Axon that can chain 2 axons together, like Iterator::chain().
/// ## Example
/// ```rust
/// assert_eq!(axon!([1., 2.], [3., 4.]).to_array(), [1., 2., 3., 4.])
/// ```
#[derive(Copy, Clone)]
pub struct AxonChain<A: Axon<A_>, B: Axon<B_>, const A_: usize, const B_: usize>(A, B);

impl<A: Axon<A_>, B: Axon<B_>, const A_: usize, const B_: usize> Index<usize>
    for AxonChain<A, B, A_, B_>
{
    type Output = Float;
    fn index(&self, n: usize) -> &Float {
        if n < A_ {
            &self.0[n]
        } else {
            debug_assert!(n < A_ + B_, "Index out of chain: {} >= {} + {}", n, A_, B_);
            &self.1[n - A_]
        }
    }
}

impl<A: Axon<A_>, B: Axon<B_>, const A_: usize, const B_: usize> Index<Range<usize>>
    for AxonChain<A, B, A_, B_>
{
    type Output = [Float];

    fn index(&self, n: Range<usize>) -> &[Float] {
        if n.start < A_ && n.end > A_ {
            panic!("Range not alligned with AxonChain")
        }

        if n.start < A_ {
            &self.0[n]
        } else {
            &self.1[n]
        }
    }
}

/// An reference to an axon that won't panic if you call zero on it.
/// This is the type returned when ```to_ref``` is called on an axon.
/// It also implements +, -, /, *, % and ```fmt::Display```
#[derive(Clone, Copy)]
pub enum AxonRef<'a, T: Axon<LEN>, const LEN: usize> {
    Ref(&'a T),
    Fake(T),
}

impl<'a, T: Axon<LEN>, const LEN: usize> Display for AxonRef<'a, T, LEN> {
    #[cfg(feature = "prettyprint")]
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let mut stdout = stdout();

        let magnitude = (0..LEN).map(|n| self[n]).fold(Float::NAN, Float::max);

        for x in 0..LEN {
            let val = self[x] / magnitude * 255.;
            let color = match val.is_sign_positive() {
                true => style::Color::Rgb {
                    r: 0,
                    g: 0,
                    b: val as u8,
                },
                false => style::Color::Rgb {
                    r: val.abs() as u8,
                    g: 0,
                    b: 0,
                },
            };
            stdout.queue(style::SetBackgroundColor(color)).unwrap();
            if val as u8 == 0 {
                stdout.queue(style::ResetColor).unwrap();
            }
            stdout.queue(style::Print("  ")).unwrap();
        }

        stdout.queue(style::ResetColor).unwrap();
        stdout.flush().unwrap();

        write!(f, "")?;
        Ok(())
    }

    #[cfg(not(feature = "prettyprint"))]
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "prettyprint is not enabled. This feature needs to be manually set in your Cargo.toml"
        )
    }
}

impl<'a, T: Axon<LEN>, const LEN: usize> Deref for AxonRef<'a, T, LEN> {
    type Target = T;

    fn deref(&self) -> &Self::Target {
        match self {
            AxonRef::Ref(r) => r,
            AxonRef::Fake(f) => f,
        }
    }
}

impl<'a, T: Axon<LEN>, const LEN: usize> Index<usize> for AxonRef<'a, T, LEN> {
    type Output = Float;
    fn index(&self, n: usize) -> &Float {
        match self {
            AxonRef::Ref(r) => &r[n],
            AxonRef::Fake(f) => &f[n],
        }
    }
}

impl<'a, T: Axon<LEN>, const LEN: usize> Index<Range<usize>> for AxonRef<'a, T, LEN> {
    type Output = [Float];
    fn index(&self, n: Range<usize>) -> &[Float] {
        match self {
            AxonRef::Ref(r) => &r[n],
            AxonRef::Fake(f) => &f[n],
        }
    }
}

macro_rules! impl_math{
    ($name: ident, $fn: ident, $op: tt) => {
        impl<'a, T: Axon<LEN>, const LEN: usize> $name<AxonRef<'a, T, LEN>> for AxonRef<'a, T, LEN>{
            type Output = [Float; LEN];
            fn $fn(self, other: Self) -> [Float; LEN]{
                let mut tmp = [0.; LEN];
                for n in 0..LEN{
                    tmp[n] = self[n] $op other[n];
                }
                tmp
            }
        }
    }
}

impl_math!(Sub, sub, -);
impl_math!(Add, add, +);
impl_math!(Mul, mul, *);
impl_math!(Div, div, /);
impl_math!(Rem, rem, %);

/// LazyAxon will let you lazily cast any slice as an axon
///
/// ## Example
///
/// ```rust
/// assert_eq!(LazyAxon<2>::new(&[1., 2., 3.]).to_array(), [1., 2.])
/// // This is fine. (keep in mind that to_array is slow)
/// ```
///
/// ```rust
/// LazyAxon<4>::new(&[1., 2., 3.]) // This will panic
/// ```
///
/// ```rust
/// LazyAxon<3>::new(&[1., 2., 3.]) // And this is correct use
/// ```
#[derive(Clone, Copy)]
pub struct LazyAxon<'a, const LEN: usize>(pub &'a [Float]);

impl<'a, const LEN: usize> LazyAxon<'a, LEN> {
    pub fn new(a: &'a [Float]) -> Self {
        assert!(
            a.len() >= LEN,
            "LazyAxon can only be made from data of same legth, or longer, then the axon"
        );
        Self(a)
    }
}

impl<'a, const LEN: usize> Index<usize> for LazyAxon<'a, LEN> {
    type Output = Float;
    fn index(&self, n: usize) -> &Float {
        &self.0[n]
    }
}

impl<'a, const LEN: usize> Index<Range<usize>> for LazyAxon<'a, LEN> {
    type Output = [Float];
    fn index(&self, n: Range<usize>) -> &[Float] {
        &self.0[n]
    }
}

impl<A: Axon<A_>, B: Axon<B_>, const A_: usize, const B_: usize> Axon<{ A_ + B_ }>
    for AxonChain<A, B, A_, B_>
{
    fn zero() -> Self {
        AxonChain(A::zero(), B::zero())
    }

    //fn simd(&self, n: usize) -> Simd<[Float; SIMD_LANES]>{
    //    if n+SIMD_LANES < A_{
    //        self.0.simd(n)
    //    }else if n > A_{
    //        let mut buffer = [0.; SIMD_LANES];
    //        for cp in 0..SIMD_LANES{
    //            buffer[cp] = self[n+cp]
    //        }
    //        Simd::<[Float; SIMD_LANES]>::from_slice_unaligned(&buffer[..])
    //    }else{
    //        self.1.simd(n-A_)
    //    }
    //}
}

impl<const LEN: usize> Axon<LEN> for [Float; LEN] {
    fn zero() -> Self {
        [0.; LEN]
    }
    //fn simd(&self, n: usize) -> Simd<[Float; SIMD_LANES]>{
    //    Simd::<[Float; SIMD_LANES]>::from_slice_unaligned(&self[n..])
    //}
}

impl<'a, T: Axon<LEN>, const LEN: usize> Axon<LEN> for AxonRef<'a, T, LEN> {
    fn zero() -> Self {
        AxonRef::Fake(T::zero())
    }

    //fn simd(&self, n: usize) -> Simd<[Float; SIMD_LANES]>{
    //    match self{
    //        AxonRef::Ref(r)  => r.simd(n),
    //        AxonRef::Fake(f) => f.simd(n),
    //    }
    //}
}

impl<'a, const LEN: usize> Axon<LEN> for LazyAxon<'a, LEN> {
    fn zero() -> Self {
        panic!("You can't lazily make an axon")
    }
    //fn simd(&self, n: usize) -> Simd<[Float; SIMD_LANES]>{
    //    Simd::<[Float; SIMD_LANES]>::from_slice_unaligned(&self.0[n..])
    //}
}

//pub const fn paddet_lanes(len: usize, lanes: usize) -> usize{
//    if len % lanes == 0{
//        len/lanes
//    }else{
//        len/lanes + 1
//    }
//}

//extern crate stdsimd;
//use stdsimd::simd::f32x4;

//#[derive(Clone, Copy)]
//pub struct PaddetSimdAxon<const LEN: usize, const LANES: usize>
//where [[Float; LANES]; paddet_lanes(LEN, LANES)]: Sized{
//    buffer: [[Float; LANES]; paddet_lanes(LEN, LANES)]
//}
//
//impl<const LEN: usize, const LANES: usize> Index<usize> for PaddetSimdAxon<LEN, LANES>
//where [[Float; LANES]; paddet_lanes(LEN, LANES)]: Sized{
//    type Output = Float;
//    fn index(&self, n: usize) -> &Float{
//        &self.buffer[n/4][n%4]
//    }
//}
//
//impl<const LEN: usize, const LANES: usize> Axon<LEN> for PaddetSimdAxon<LEN, LANES>
//where [[Float; LANES]; paddet_lanes(LEN, LANES)]: Sized{
//    fn zero() -> Self{
//        Self{buffer: [[0.;LANES];paddet_lanes(LEN, LANES)]}
//    }
//}

// Layer trait

/// This trait needs to be implemented to expose needed types to the proc macro, without knowing
/// the input and output size of the layer.
pub trait LayerTy {
    type Gradient;
    type Output;
}

pub trait Layer<const I_LEN: usize, const O_LEN: usize>: LayerTy
where
    <Self as LayerTy>::Gradient: Axon<I_LEN>,
    <Self as LayerTy>::Output: Axon<O_LEN>,
{
    const O_LEN: usize = O_LEN;
    const I_LEN: usize = I_LEN;

    fn predict(&mut self, i: impl Axon<I_LEN>) -> Result<Self::Output>; // TODO: Make predict consume self, and use wrappers for mutable and imutable calls.
    fn backpropagate(
        &mut self,
        i: impl Axon<I_LEN>,
        gradient: impl Axon<O_LEN>,
    ) -> Result<Self::Gradient>;
}

/// A simple neuron.
/// Should be initialized with Self::const_xavier(), and not random.
/// Random should only be used if (for some reason) you dont know the output size of your layer at compile time.
#[derive(Clone, Copy, Debug)]
pub struct Neuron<const LEN: usize> {
    pub w: [Float; LEN],
    pub b: Float,
}

impl<const LEN: usize> Neuron<LEN> {
    pub fn random() -> Self {
        let mut buffer = [0.; LEN];
        buffer.iter_mut().for_each(|n| *n = random());
        Self {
            w: buffer,
            b: random(),
        }
    }

    /// Same as random, but should lower the chance of vanishing/ exploding gradiants.
    pub fn const_xavier<const OUT: usize>() -> Self {
        let xavier =
            || (random::<Float>() - 0.5) * 2. * (2. / (LEN as Float + OUT as Float)).sqrt();
        let mut buffer = [0.; LEN];
        buffer.iter_mut().for_each(|n| *n = xavier());
        Self {
            w: buffer,
            b: xavier(),
        }
    }

    pub fn zero() -> Self {
        let buffer = [0.; LEN];
        Self { w: buffer, b: 0. }
    }

    /// Inputs, times weights, add a bias, activate.
    pub fn predict<T: Axon<LEN>>(&self, i: T) -> Float {
        let mut sum = 0.;
        for n in 0..LEN {
            sum += i[n] * self.w[n]
        }
        sum + self.b

        //let mut sum = Simd::<[Float; SIMD_LANES]>::splat(0.);
        //for n in (LEN%SIMD_LANES..LEN).step_by(SIMD_LANES){
        //    sum += self.w.simd(n) * i.simd(n)
        //}
        //sum.sum() + self.b + (0..LEN%SIMD_LANES).map(|n| i[n] * self.w[n]).sum::<Float>()
    }

    pub fn backpropagate<T: Index<usize, Output = Float>>(
        &mut self,
        i: T,
        delta: Float,
        lr: Float,
    ) -> Result<[Float; LEN]> {
        if delta.is_infinite() || delta.is_nan() {
            bail!("Exploding/vanishing gradient\nΔ = {}", delta)
        }

        self.b -= delta * lr;
        if self.b.is_infinite() || self.b.is_nan() {
            bail!("Exploding/vanishing gradient in bias")
        }

        let mut gradient = self.w; // [0.; LEN];

        for n in 0..LEN {
            gradient[n] *= delta;
            self.w[n] -= delta * i[n] * lr;
            //gradient[n] = self.w[n] * delta;
            if self.w[n].is_infinite() || self.w[n].is_nan() {
                bail!(
                    "Exploding/Vanishing gradient in weight {}\n input = {}\n Δ = {}\n w = {}",
                    n,
                    i[n],
                    delta,
                    self.w[n]
                )
            }
        }

        Ok(gradient)
    }
}

#[derive(Clone, Copy)]
pub struct DenseLayer<const I_LEN: usize, const O_LEN: usize> {
    pub neurons: [Neuron<I_LEN>; O_LEN],
    pub lr: Float,
}

impl<const I_LEN: usize, const O_LEN: usize> DenseLayer<I_LEN, O_LEN> {
    pub fn random(lr: Float) -> Self {
        let mut buffer = [Neuron::random(); O_LEN];
        buffer
            .iter_mut()
            .for_each(|n| *n = Neuron::const_xavier::<O_LEN>());
        Self {
            neurons: buffer,
            lr,
        }
    }
}

impl<const I_LEN: usize, const O_LEN: usize> LayerTy for DenseLayer<I_LEN, O_LEN> {
    type Gradient = [Float; I_LEN];
    type Output = [Float; O_LEN];
}

impl<const I_LEN: usize, const O_LEN: usize> Layer<I_LEN, O_LEN> for DenseLayer<I_LEN, O_LEN> {
    fn predict(&mut self, i: impl Axon<I_LEN>) -> Result<[Float; O_LEN]> {
        let mut buffer = [0.; O_LEN];

        buffer
            .par_iter_mut()
            .enumerate()
            .for_each(|(n, o)| *o = self.neurons[n].predict(axon!(ref &i)));

        Ok(buffer)
    }

    fn backpropagate(
        &mut self,
        i: impl Axon<I_LEN>,
        gradient: impl Axon<O_LEN>,
    ) -> Result<[Float; I_LEN]> {
        let mut matrix = [[0.; I_LEN]; O_LEN];
        let lr = self.lr;

        self.neurons
            .par_iter_mut()
            .zip(matrix.par_iter_mut())
            .enumerate()
            .map(|(n, (neuron, o))| {
                *o = neuron
                    .backpropagate(axon!(ref &i), gradient[n], lr)
                    .context(format!("Error in neuron {}", n))?;
                Ok(())
            })
            .collect::<Result<()>>()?;

        let mut buffer = [0.; I_LEN];
        buffer.par_iter_mut().enumerate().for_each(|(x, o)| {
            for y in 0..O_LEN {
                *o += matrix[y][x]
            }
        });

        Ok(buffer)
    }
}
