use exotic_macro::*;
use exotic::{
    deep::{
        activation::*,
        DenseLayer,
        Axon,
        Layer,
        LazyAxon,
    },
    self
};
use anyhow::*;
use rand::*;
use std::f32::consts::E;

pub fn main() -> Result<()>{
    model!((
        name: "Net",
        cached: true,
        layers: [
            ("DenseLayer", [1, 3]),
            ("Swish", [3]),
            ("DenseLayer", [3, 2]),
            ("Softmax", [2]),
        ]
    ));

    let mut net = Net{
        l0: DenseLayer::random(0.1),
        l1: Swish,               
        l2: DenseLayer::random(0.1),
        l3: Softmax,
        cache: Net::new_cache(),
    };
    
    for _ in 0..10{
        //let a = (random::<f32>() - 0.5) * 2.0;
        //let b = (random::<f32>() - 0.5) * 2.0;
        //let f = |x: f32| a*x+b;
        //println!("f(x) = {}x + {}", a,b);
        let y = if random(){[1., 0.]}else{[0., 1.]};
        for n in 0..100{
            let i = random();
            let o = net.cache([i])?;
            if n % 20 == 0{println!("{:.4}", (y.to_ref() - o).iter().map(|n|n.powi(2)).sum::<f32>());}
            let d = o - y.to_ref();
            net.backpropagate_from_cache([i], d)?;
        }
        println!()
    }

    let s = [1., random::<f32>()-0.5, random::<f32>()-0.5];
    println!("{:?} -> {:?} or {:?}", s, Softmax.predict(s)?.to_array(), s.iter().map(|n|n/s.iter().map(|n|n.exp()).sum::<f32>()).collect::<Vec<_>>());
    println!("{} == {}", E.powf(s[1]), s[1].exp());

    Ok(())
}
