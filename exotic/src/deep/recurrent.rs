use crate::deep::*;
//use rayon::prelude::*;
use std::ops::Index;

#[derive(Clone, Copy)]
pub struct DenseLayer<const I_LEN: usize, const O_LEN: usize>
where
    Neuron<{ I_LEN + O_LEN }>: Sized,
{
    neurons: [Neuron<{ I_LEN + O_LEN }>; O_LEN],
    pub buffer: [Float; O_LEN],
    lr: Float,
}

#[derive(Copy, Clone)]
struct LazyChain<'a>(&'a [Float], &'a [Float]);
impl<'a> Index<usize> for LazyChain<'a> {
    type Output = Float;
    fn index(&self, n: usize) -> &Float {
        if n < self.0.len() {
            &self.0[n]
        } else {
            &self.1[n - self.0.len()]
        }
    }
}

impl<'a> Index<Range<usize>> for LazyChain<'a> {
    type Output = [Float];
    fn index(&self, n: Range<usize>) -> &[Float] {
        if n.start < self.0.len() && n.end > self.0.len() {
            panic!("Range not alligned with recurrent::LazyChain")
        }

        if n.start < self.0.len() {
            &self.0[n]
        } else {
            &self.1[n]
        }
    }
}

impl<'a, const LEN: usize> Axon<LEN> for LazyChain<'a> {
    fn zero() -> Self {
        panic!("You can't lazily make an axon")
    }
}

impl<const I_LEN: usize, const O_LEN: usize> DenseLayer<I_LEN, O_LEN>
where
    Neuron<{ I_LEN + O_LEN }>: Sized,
{
    pub fn random(lr: Float) -> Self {
        let mut buffer = [Neuron::<{ I_LEN + O_LEN }>::random(); O_LEN];
        buffer
            .iter_mut()
            .for_each(|n| *n = Neuron::<{ I_LEN + O_LEN }>::const_xavier::<O_LEN>());
        Self {
            neurons: buffer,
            buffer: [0.; O_LEN],
            lr,
        }
    }

    pub fn memorize<T: Axon<I_LEN>>(&mut self, i: T) -> Result<()>
    where
        AxonChain<[f32; O_LEN], T, O_LEN, I_LEN>: Axon<{ I_LEN + O_LEN }>,
    {
        let mut buffer = [0.; O_LEN];

        let bruh = axon!(self.buffer, i);
        for (n, o) in buffer.iter_mut().enumerate() {
            *o = self.neurons[n].predict(bruh)
        }

        self.buffer = buffer;
        Ok(())
    }

    pub fn memorize_par<T: Axon<I_LEN>>(&mut self, i: T) -> Result<()>
    where
        AxonChain<[f32; O_LEN], T, O_LEN, I_LEN>: Axon<{ I_LEN + O_LEN }>,
    {
        let mut buffer = [0.; O_LEN];

        let bruh = axon!(self.buffer, i);
        buffer
            .par_iter_mut()
            .enumerate()
            .for_each(|(n, o)| *o = self.neurons[n].predict(bruh));

        self.buffer = buffer;
        Ok(())
    }

    pub fn forget(&mut self) {
        self.buffer = [0.; O_LEN];
    }
}

impl<const I_LEN: usize, const O_LEN: usize> LayerTy for DenseLayer<I_LEN, O_LEN>
where
    Neuron<{ I_LEN + O_LEN }>: Sized,
{
    type Gradient = [Float; I_LEN];
    type Output = [Float; O_LEN];
}

impl<const I_LEN: usize, const O_LEN: usize> Layer<I_LEN, O_LEN> for DenseLayer<I_LEN, O_LEN>
where
    Neuron<{ I_LEN + O_LEN }>: Sized,
{
    fn predict(&mut self, i: impl Axon<I_LEN>) -> Result<[Float; O_LEN]> {
        let mut buffer = [0.; O_LEN];

        for n in 0..O_LEN {
            buffer[n] = self.neurons[n].predict(LazyChain(&self.buffer, i.slice()))
        }

        Ok(buffer)
    }

    fn backpropagate(
        &mut self,
        i: impl Axon<I_LEN>,
        gradient: impl Axon<O_LEN>,
    ) -> Result<[Float; I_LEN]> {
        let mut buffer = [0.; I_LEN];

        for n in 0..O_LEN {
            let d = self.neurons[n]
                .backpropagate(LazyChain(&self.buffer, i.slice()), gradient[n], self.lr)
                .context(format!("Error in neuron {}", n))?;
            for n in 0..I_LEN {
                buffer[n] += d[n]
            }
        }

        Ok(buffer)
    }
}
