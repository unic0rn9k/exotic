#![feature(test)]

use exotic::{
    config::SIMD_LANES,
    deep::{activation::*, Axon, DenseLayer, Layer},
    Context, Result,
};
use exotic_macro::*;
use packed_simd_2::Simd;
use rand::*;

extern crate test;

model!((
    name: "TestNet",
    cached: true,
    layers: [
        ("DenseLayer", [1, 100_000]),
        ("Swish", [100_000]),
        ("DenseLayer", [100_000, 1]),
        ("Tanh", [1])
    ]
));

#[cfg(test)]
mod tests {
    use super::*;
    use test::Bencher;

    #[bench]
    fn main(bench: &mut Bencher) {
        let mut test_net = TestNet {
            l0: DenseLayer::random(0.001),
            l1: Swish,
            l2: DenseLayer::random(0.001),
            l3: Tanh,
            cache: TestNet::new_cache(),
        };

        let a: f32 = random();
        let b: f32 = random();
        let f = |x: f32| x * a + b;

        bench.iter(|| {
            let x = random();
            let o = test_net.cache([x]).unwrap();
            let d = o[0] - f(x);

            //test_net.backpropagate_from_cache([x], [d]).unwrap();
        });
    }

    #[bench]
    fn simd_dot(bench: &mut Bencher) {
        const LEN: usize = 10_000;
        let mut a = [0.; LEN];
        let mut b = [0.; LEN];
        for n in 0..LEN {
            a[n] = random();
            b[n] = random();
        }

        bench.iter(|| {
            let mut sum = Simd::<[f32; SIMD_LANES]>::splat(0.);
            for n in (LEN % SIMD_LANES..LEN).step_by(SIMD_LANES) {
                sum += a.simd(n) * b.simd(n)
            }
            sum.sum() + (0..LEN % SIMD_LANES).map(|n| a[n] * b[n]).sum::<f32>()
        });
    }

    #[bench]
    fn dot(bench: &mut Bencher) {
        const LEN: usize = 10_000;
        let mut a = [0.; LEN];
        let mut b = [0.; LEN];
        for n in 0..LEN {
            a[n] = random();
            b[n] = random();
        }

        bench.iter(|| {
            let mut sum = 0.;
            for n in 0..LEN {
                sum += a[n] * b[n]
            }
            sum
        });
    }
}
