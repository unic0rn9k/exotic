/// This module is still in its early stage, so the code is a bit spagetti
use crate::{config::Float, deep::*};
use num_complex::Complex;
use rustfft::*;
use std::marker::PhantomData;

pub trait MutComplexMatrix:
    Index<usize, Output = [Complex<Float>]> + IndexMut<usize, Output = [Complex<Float>]>
{
    fn width(&self) -> usize;
    fn height(&self) -> usize;

    fn assert_dim(&self, w: usize, h: usize) {
        debug_assert_eq!(self.width(), w);
        debug_assert_eq!(self.height(), h);
    }

    /// Should always return the smallest possible matrix of type Self.
    fn zero() -> Self;

    fn transpose(&self, buffer: &mut impl MutComplexMatrix) {
        for x in 0..self.width() {
            for y in 0..self.height() {
                buffer[y][x] = self[x][y]
            }
        }
    }

    /// Multiply 2 matrecies and write the result to buffer.
    fn mul(&self, other: &impl MutComplexMatrix, buffer: &mut impl MutComplexMatrix) {
        let w = self.width();
        let h = self.height();
        let k = other.height();

        assert!(
            buffer.width() == w && buffer.height() == k,
            "Mal-shaped buffer."
        );
        assert!(other.width() == h, "Can only multiply a matricies where the height of the first is equal to the width of the other");

        for k in 0..w {
            for i in 0..h {
                for j in 0..k {
                    buffer[i][j] += self[i][k] * other[k][j]
                }
            }
        }
    }
}

/// This struct makes it posible to return a complex matrix as an axon.
/// This will likely be changed in the future, maybe MutComplexMatrix will depend on Axon.
#[derive(Clone, Copy)]
pub struct LazyMutComplexMatrixAxon<T: MutComplexMatrix, const LEN: usize>(T);

impl<T: MutComplexMatrix, const LEN: usize> Index<usize> for LazyMutComplexMatrixAxon<T, LEN> {
    type Output = Float;
    fn index(&self, n: usize) -> &Float {
        let x = n % self.0.width();
        let y = (n - x) / self.0.width();
        &self.0[x][y].re
    }
}

/// This trait cannot be implemented, as taking a slice from a MutComplexMatrix returns &[Complex<Float>]
impl<T: MutComplexMatrix, const LEN: usize> Index<Range<usize>>
    for LazyMutComplexMatrixAxon<T, LEN>
{
    type Output = [Float];
    fn index(&self, _: Range<usize>) -> &[Float] {
        unimplemented!()
    }
}

impl<T: MutComplexMatrix + Copy + Sync, const LEN: usize> Axon<LEN>
    for LazyMutComplexMatrixAxon<T, LEN>
{
    fn zero() -> Self {
        panic!("MutComplexMatrix can't be lazily initialized")
    }
}

/// A statically allocated matrix of complex numbers (used for convolution).
#[derive(Clone, Copy)]
pub struct ComplexMatrix<const W: usize, const H: usize>([[Complex<Float>; H]; W]);

impl<const W: usize, const H: usize> ComplexMatrix<W, H> {
    pub const fn len(&self) -> usize {
        W * H
    }
}

impl<const W: usize, const H: usize> Index<usize> for ComplexMatrix<W, H> {
    type Output = [Complex<Float>];
    fn index(&self, n: usize) -> &[Complex<Float>] {
        &self.0[n]
    }
}

impl<const W: usize, const H: usize> IndexMut<usize> for ComplexMatrix<W, H> {
    fn index_mut(&mut self, n: usize) -> &mut [Complex<Float>] {
        &mut self.0[n]
    }
}

impl<const W: usize, const H: usize> const MutComplexMatrix for ComplexMatrix<W, H> {
    fn width(&self) -> usize {
        W
    }
    fn height(&self) -> usize {
        H
    }

    fn zero() -> Self {
        //assert!(
        //    w == W && h == H,
        //    "Cannot create ComplexMatrix with dimenstions that do not match type"
        //);
        Self([[Complex::<Float> { re: 0., im: 0. }; H]; W])
    }
}

/// ConvLayer will not implement Copy in the future.
#[derive(Clone, Copy)]
pub struct ConvLayer<
    Kernel: MutComplexMatrix,
    Trans: MutComplexMatrix,
    Output: MutComplexMatrix,
    const I_LEN: usize,
    const O_LEN: usize,
> {
    kernel: Kernel,
    lr: Float,
    _trans: PhantomData<Trans>,
    _output: PhantomData<Output>,
}

/// This function returns a conv layer, based on the witdth and height of its kernel.
/// This is not implemented for the ConvLayer struct, as this would then require the exact type of
/// the conv layer to be known, which differs based on its dimensions, and type of matrix used.
pub fn random_conv_layer<const W: usize, const H: usize>(
    lr: Float,
) -> ConvLayer<ComplexMatrix<W, H>, ComplexMatrix<W, H>, ComplexMatrix<W, H>, { W * H }, { W * W }>
{
    ConvLayer {
        kernel: ComplexMatrix::zero(),
        lr,
        _trans: PhantomData,
        _output: PhantomData,
    }
}

impl<
        K: MutComplexMatrix,
        T: MutComplexMatrix,
        O: MutComplexMatrix,
        const I_LEN: usize,
        const O_LEN: usize,
    > LayerTy for ConvLayer<K, T, O, I_LEN, O_LEN>
{
    type Output = LazyMutComplexMatrixAxon<O, O_LEN>;
    type Gradient = LazyMutComplexMatrixAxon<K, I_LEN>;
}

impl<
        Kernel: MutComplexMatrix + Copy + Sync,
        Trans: MutComplexMatrix,
        Output: MutComplexMatrix + Copy + Sync,
        const I_LEN: usize,
        const O_LEN: usize,
    > Layer<I_LEN, O_LEN> for ConvLayer<Kernel, Trans, Output, I_LEN, O_LEN>
{
    fn predict(&mut self, i: impl Axon<I_LEN>) -> Result<Self::Output> {
        // Fft of input

        let height = self.kernel.height();

        let mut planner = FftPlanner::<f32>::new();
        let fft = planner.plan_fft_forward(height);

        let mut input_buffer = Kernel::zero();
        let mut kernel_buffer = self.kernel;

        for n in 0..input_buffer.width() {
            input_buffer[n]
                .iter_mut()
                .zip(i[n * height..(n + 1) * height].iter())
                .for_each(|(o, i)| *o = Complex { re: *i, im: 0. });
            fft.process(&mut input_buffer[n]);
            fft.process(&mut kernel_buffer[n]);
        }

        let mut trans_input_buffer = Trans::zero();
        let mut trans_kernel_buffer = Trans::zero();

        input_buffer.transpose(&mut trans_input_buffer);
        kernel_buffer.transpose(&mut trans_kernel_buffer);

        let fft = planner.plan_fft_forward(trans_kernel_buffer.height());

        for n in 0..trans_kernel_buffer.width() {
            fft.process(&mut trans_kernel_buffer[n]);
            fft.process(&mut trans_input_buffer[n])
        }

        trans_input_buffer.transpose(&mut input_buffer);
        //trans_kernel_buffer.transpose(&mut kernel_buffer);

        let mut out_buffer = Output::zero();
        trans_kernel_buffer.mul(&input_buffer, &mut out_buffer);

        // return inverse fft of out_buffer.

        let out_buffer_size = out_buffer.width();
        // out_buffer is always square, and therefore its width can be used in place of its height

        let fft = planner.plan_fft_inverse(out_buffer_size);

        for n in 0..out_buffer_size {
            fft.process(&mut out_buffer[n])
        }

        out_buffer.clone().transpose(&mut out_buffer);

        let fft = planner.plan_fft_inverse(out_buffer_size);

        for n in 0..out_buffer_size {
            fft.process(&mut out_buffer[n])
        }

        //It may be necessaty to transpose outbuffer again.

        Ok(LazyMutComplexMatrixAxon(out_buffer))
    }

    fn backpropagate(
        &mut self,
        _i: impl Axon<I_LEN>,
        _d: impl Axon<O_LEN>,
    ) -> Result<Self::Gradient> {
        unimplemented!()
    }
}

#[test]
fn ok() {
    let mut l = random_conv_layer::<10, 10>(0.);
    l.predict([0.; 10 * 10]).unwrap();
}
