use anyhow::*;
//use rand::*;
//use image::*;
//use lazy_static::*;
use exotic::{matrix::*, deep::Axon};
use rustfft::*;
use num_complex::Complex;

//use crossterm::{
//    ExecutableCommand, QueueableCommand,
//    terminal, cursor, style::{self, Colorize}
//};

const S: usize = 11;

fn transpose(m: &[[Complex::<f32>; S]; S]) -> [[Complex::<f32>;S];S]{
    let mut tmp = [[Complex{im:0.,re:0.};S];S];
    for x in 0..S{
        for y in 0..S{
            tmp[x][y] = m[y][x]
        }
    }
    tmp
}

fn to_fourier(m: &[[f32;S];S]) -> [[Complex<f32>;S];S]{
 
    let mut tmp = [[Complex{im:0.,re:0.};S];S];
    for x in 0..S{
        for y in 0..S{
            tmp[x][y] = Complex{re: m[x][y], im: m[x][y] / 2.}
        }
    }
    let mut planner = FftPlanner::<f32>::new();
    let fft = planner.plan_fft_forward(S);

    for x in 0..S{
        fft.process(&mut tmp[x]);
    }
    tmp = transpose(&tmp);
    for x in 0..S{
        fft.process(&mut tmp[x]);
    }

    tmp
}

fn from_fourier(mut m: [[Complex::<f32>;S];S]) -> [[Complex<f32>;S];S]{
    let mut planner = FftPlanner::<f32>::new();
    let fft = planner.plan_fft_inverse(S);

    m = transpose(&m);
    for x in 0..S{
        fft.process(&mut m[x]);
    }
    m = transpose(&m);
    for x in 0..S{
        fft.process(&mut m[x]);
    }

    m
}

pub fn main() -> Result<()>{
    //println!("{}", "Exotic Dream".yellow());
    //let img = image::open("example.png")?;
    //println!("dimensions {:?}", img.dimensions());

    //println!("{:?"}, I!(const [2,2]));
    
    let mut buffer = [
        [0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.],
        [0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.],
        [0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.],
        [0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.],
        [0.,0.,0.,0.,0.,1.,0.,0.,0.,0.,0.],
        [0.,0.,0.,0.,0.,1.,0.,0.,0.,0.,0.],
        [0.,0.,0.,0.,0.,1.,0.,0.,0.,0.,0.],
        [0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.],
        [0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.],
        [0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.],
        [0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.],
    ];

    let kernel = to_fourier(&[
        [0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.],
        [0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.],
        [0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.],
        [0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.],
        [0.,0.,0.,0.,1.,1.,1.,0.,0.,0.,0.], 
        [0.,0.,0.,0.,1.,0.,1.,0.,0.,0.,0.],
        [0.,0.,0.,0.,1.,1.,1.,0.,0.,0.,0.],
        [0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.],
        [0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.],
        [0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.],
        [0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.],
    ]);

    //let mut cos = [[0.;S];S];
    //for x in 0..S{
    //    for y in 0..S{
    //        cos[y][x] = (x as f32).sin();
    //        println!("{}", cos[y][x]);
    //    }
    //}
    //
    //matrix![&cos.iter().flatten().map(|n|*n).collect::<Vec<_>>()[..], I![const S, S]].heatmap(1.)?;
    //matrix![&transpose(&to_fourier(&cos)).iter().flatten().map(|n|n.re).collect::<Vec<_>>()[..], I![const S, S]].heatmap(20.)?;


    for _ in 0..4{
        let board = matrix![&buffer.iter().flatten().map(|n|*n).collect::<Vec<_>>()[..], I![const S, S]];
        board.heatmap(1.)?;
        
        //let tmp = conv_flat(
        //    &buffer,
        //    &kernel,
        //);
        //drop(board);
        //buffer = MatrixSlice::<2, 49>{
        //    data: &tmp,
        //    shape: I![const 13, 7],
        //    slice: I![const 7, 7],
        //}.to_array();

        //let mut tmp = [0.; BOARD_DIM.volume()];
        //for x in 0..BOARD_DIM.0[0]{
        //    for y in 0..BOARD_DIM.0[1]{
        //        tmp[y*BOARD_DIM.0[0]+x]=buffer[y+(BOARD_DIM.0[0]-1-x)*BOARD_DIM.0[0]]
        //    }
        //}
        //buffer=tmp;

        let mut tmp = to_fourier(&buffer);
        for x in 0..S{
            for y in 0..S{
                tmp[x][y] *= kernel[x][y]
            }
        }
        tmp = from_fourier(tmp);
        for x in 0..S{
            for y in 0..S{
                buffer[x][y] = tmp[x][y].im
            }
        }
    }

    Ok(())
}
