#![allow(incomplete_features)]
#![feature(
    stdsimd,
    const_evaluatable_checked,
    const_generics,
//    const_fn,
    const_trait_impl,
    impl_trait_in_bindings,
    const_panic,
)]

//#![feature(inherent_associated_types)]
//#![feature(const_panic)]
//#![recursion_limit = "512"]
#[macro_use]
pub mod config;
pub mod deep;
pub mod prelude;
pub mod serialization;

#[cfg(feature = "prettyprint")]
#[macro_use]
#[cfg(feature = "prettyprint")]
pub mod matrix;

pub use anyhow;
pub use anyhow::*;

pub const fn onehot<const LEN: usize>(n: usize) -> [config::Float; LEN] {
    let mut tmp = [0.; LEN];
    tmp[n] = 1.;
    tmp
}
