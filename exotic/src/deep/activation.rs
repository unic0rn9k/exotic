use crate::config::{Float, FLOAT_BYTES};
use crate::deep::{Axon, Layer, LayerTy};
use crate::serialization::Serialization;
use anyhow::*;

/// Simple macro for creating simple activation functions.
/// ### Example
/// Example implementation of an approximation of the sigmoid activation function.
/// ```
/// activation!{
///    Sigmoid =
///        |x| x/(x + 1.),
///        |x| 1. / (x.powi(2) + 2. * x + 1.)
/// }
/// ```
#[macro_export]
macro_rules! activation {
    ($name: ident = $f: expr, $d: expr $(,)?) => {
        #[derive(Clone, Copy)]
        pub struct $name<const LEN: usize>;

        impl<const LEN: usize> LayerTy for $name<LEN> {
            type Gradient = [Float; LEN];
            type Output = [Float; LEN];
        }

        impl<const LEN: usize> Layer<LEN, LEN> for $name<LEN> {
            fn predict(&mut self, i: impl Axon<LEN>) -> Result<[Float; LEN]> {
                let mut buffer = [0.; LEN];
                for n in 0..LEN {
                    buffer[n] = $f(i[n])
                }
                Ok(buffer)
            }
            fn backpropagate(
                &mut self,
                i: impl Axon<LEN>,
                gradient: impl Axon<LEN>,
            ) -> Result<[Float; LEN]> {
                let mut buffer = [0.; LEN];
                for n in 0..LEN {
                    buffer[n] = $d(i[n]) * gradient[n]
                }
                Ok(buffer)
            }
        }

        impl<const LEN: usize> Serialization<0> for $name<LEN> {
            fn serialize_into(&self, _: &mut [[u8; FLOAT_BYTES]]) {}
            fn deserialize(&mut self, _: &mut dyn std::iter::Iterator<Item = u8>) {}
        }
    };
}

pub fn sigmoid(x: Float) -> Float {
    1. / (1. + (-x).exp())
}

activation! {
    Sigmoid =
        |x| sigmoid(x),
        |x| sigmoid(1. - x),
}

activation! {
    Tanh =
        |x: Float| x.tanh(),
        |x: Float| (-x.tanh().powi(2)) + 1.,
}

// x \cdot \sigma(x)
// \sigma(x) * (1 + x * (1 - \sigma(x)))
activation! {
    Swish =
        |x: Float| x / ((-x).exp() + 1.),
        |x| {
            let sig = sigmoid(x);
            sig * (1. + x * (1. - sig))
        }
}

activation! {
    Relu =
        |x: Float| x.max(0.),
        |x| (x>0.) as u8 as Float,
}

activation! {
    None =
        |x| x,
        |_| 1.,
}

#[derive(Clone, Copy)]
pub struct Softmax<const LEN: usize>;

impl<const LEN: usize> LayerTy for Softmax<LEN> {
    type Gradient = [Float; LEN];
    type Output = [Float; LEN];
}

impl<const LEN: usize> Layer<LEN, LEN> for Softmax<LEN> {
    fn predict(&mut self, i: impl Axon<LEN>) -> Result<[Float; LEN]> {
        let mut buffer = [0.; LEN];
        let sum: Float = (0..LEN).map(|n| i[n].exp()).sum();
        for n in 0..LEN {
            buffer[n] = i[n].exp() / sum
        }
        Ok(buffer)
    }
    fn backpropagate(
        &mut self,
        i: impl Axon<LEN>,
        gradient: impl Axon<LEN>,
    ) -> Result<[Float; LEN]> {
        let mut buffer = [0.; LEN];
        let sum: Float = (0..LEN).map(|n| i[n].exp()).sum();
        for n in 0..LEN {
            buffer[n] = i[n].exp() / sum;
            buffer[n] = buffer[n] * (1. - buffer[n]) * gradient[n]
        }
        Ok(buffer)
    }
}
