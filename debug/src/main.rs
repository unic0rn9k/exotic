//#![feature(const_fn, associated_type_bounds)]
#![allow(incomplete_features)]
#![feature(const_generics)]
#![feature(trivial_bounds)]

use std::env::args;

#[macro_use]
extern crate exotic;
extern crate exotic_macro;

use exotic::prelude::*;

macro_rules! packages{
    ($($pkg: ident),* $(,)?) => {
        $(mod $pkg;)*
        fn main() -> Result<()>{
            match &if let Some(ok) = args().nth(1){ok}else{
                bail!("Expected package as argument")
            }[..]{
                $(
                    stringify!($pkg) => {
                        $pkg::main()?;
                    },
                )*
                pkg => bail!("{} is not a package", pkg)
            };
            Ok(())
        }
    }
}

packages! {
//    maze,
//    conv,
//    exotic_dream,
    mnist,
    mnist2,
//    basic,
}
