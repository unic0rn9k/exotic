use mnist::{Mnist, MnistBuilder};
use exotic_macro::*;
use exotic::{
    deep::{
        activation::*,
        DenseLayer,
        LazyAxon,
    },
    prelude::*,
    matrix,
};
//use rand::Rng;
//use rkyv::{*, ser::*};
use lazy_static::*;

lazy_static!{
    static ref IMAGE_DATA: (Vec<f32>, Vec<u8>) = {
        let Mnist { trn_img, trn_lbl, .. } = MnistBuilder::new()
            .base_path("mnist/mnist/")
            .label_format_digit()
            .training_set_length(60_000)
            //.download_and_extract()
            .finalize();
        (trn_img.iter().map(|n| *n as f32 / 255.).collect(), trn_lbl)
    };
}

model!((
    name: "Mnet",
    cached: true,
    serialization: Some(true),
    embedable: Some("mnist2.exotic"),
    layers: [
        ("DenseLayer", [784, 290]),
        ("Swish", [290]),
        ("DenseLayer", [290, 10]),
        ("Tanh", [10]),
        ("DenseLayer", [10, 130]),
        ("Swish", [130]),
        ("DenseLayer", [130, 784]),
        ("Sigmoid", [784]),
    ]
));

pub fn main() -> Result<()>{
    let mut mnet = Mnet{
        l0: DenseLayer::random(0.001),
        l1: Swish,                  
        l2: DenseLayer::random(0.001),
        l3: Tanh,                  
        l4: DenseLayer::random(0.001),
        l5: Swish,                  
        l6: DenseLayer::random(0.003),
        l7: Sigmoid,
        cache: Mnet::new_cache(),
    };
    mnet.embed();
    //mnet.deserialize(&mut std::fs::read("mnist2.exotic")?.iter().map(|n|*n));

    const SIZE: usize = 28*28;

    let mut cost = 0.;

    for epoch in 0..69420{
        let n = epoch % 60_000;
        let i = &IMAGE_DATA.0[n*SIZE..(n+1)*SIZE];

        let o = mnet.cache(LazyAxon::<{Mnet::I_LEN}>(i))?.to_array();
        //println!("{:?}", y);

        let mut dcost = [0.;SIZE];
        for n in 0..SIZE{
            dcost[n] = o[n] - i[n];
            cost += (i[n] - o[n]).powi(2)
        }

        //mnet.backpropagate_from_cache(LazyAxon::<{Mnet::I_LEN}>(i), dcost)?;
        
        //mnet.printcache();
        if epoch % 1000 == 0{
            matrix![i,  I![const 28, 28]].heatmap(1.)?;
            matrix![&o, I![const 28, 28]].heatmap(1.)?;
            println!("cost {:.3}  epoch {}  lable {}", cost/100., epoch, IMAGE_DATA.1[n]);
            cost = 0.;
        }
        //if epoch % 10000 == 0{
        //    println!("saving...");
        //    mnet.serialize_to(&mut std::fs::File::create("mnist2.exotic")?)?;
        //}
    }

    //for n in 0..10{
    //    matrix![&mnet.l0.neurons[n].w[..], I![const 28, 28]].heatmap(0.5)?;
    //}
    
    Ok(())
}
