#[cfg(feature = "f64")]
pub type Float = f64;
#[cfg(feature = "f64")]
pub const FLOAT_BYTES: usize = 8;

#[cfg(not(feature = "f64"))]
pub const FLOAT_BYTES: usize = 4;
#[cfg(not(feature = "f64"))]
pub type Float = f32;

pub const SIMD_LANES: usize = 4;

pub const FLOAT_ROUNDING: Float = 100_000.0;

pub fn round(n: Float) -> Float {
    n.abs().min(FLOAT_ROUNDING).copysign(n)
}
